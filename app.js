var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mailer = require('express-mailer');

var app = express();
var filestorage = express();

mailer.extend(app, {
    from: 'fasttty@gmail.com',
    host: 'smtp.gmail.com', // hostname
    secureConnection: true, // use SSL
    port: 465, // port for secure SMTP
    transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
    auth: {
        user: 'fasttty@gmail.com',
        pass: ';Ost6,]x()+znHw]&I[]?x\\z1'
    }
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(favicon());
//app.use(logger('dev'));
app.use(session({secret: 'jkpwmxlkfdsarckodsega', resave: true, saveUninitialized: false }));
//app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
require('./api').callback(app);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
function devErrHandler(err, req, res, next) {
   res.status(err.status || 500);
   res.render('error', {
      message: err.message,
      error: err
   });
}

if (app.get('env') === 'development') {
   app.use(devErrHandler);
   filestorage.use(devErrHandler);
}

// production error handler
// no stacktraces leaked to user
function prodErrHandler(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
}
app.use(prodErrHandler);

module.exports = [app, filestorage];
