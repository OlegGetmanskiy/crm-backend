var db = require('pg-bricks').configure('postgres://CRMUser:aJEO7VIk0Tr4uWSiw5rD@localhost:5432/crm-demo');
var md5 = require('./md5.js');
var fs = require('fs');
var mkdir = require('mkdirp');
var multer = require('multer');
var app = null;

function sendJson(res, obj) {
   res.header("Content-Type", "application/json; charset=utf-8");
   res.end(JSON.stringify(obj, null, 3));
};

function findUserByLogin(login, callback) {
   db.select().from('crm_user').where({ login: login }).rows(function(err, data) {
      if(err) return;
      callback(data[0]);
   });
};

function findUserByEmail(email, callback) {
   db.select().from('crm_user').where({ email: email }).rows(function(err, data) {
      if(err) return;
      callback(data[0]);
   });
};

function sendWelcomeEmail(email, login, password) {
   app.mailer.send('email', {
         to: email,
         subject: 'Регистрация в системе Light Objective',
         login: login,
         password: password
      }, function (err) {
         if (err) {
         console.log(err);
         return;
      }
   });
}

function sendRecoverPasswordEmail(email, code) {
   app.mailer.send('email_recoverpw', {
         to: email,                                                                                                                                                                                         
         subject: 'Light Objective - восстановление пароля',
         code: code
      }, function (err) {
         if (err) {
         console.log(err);
         return;
      }
   });
}

function isAdmin(user) {
   return user && user.group_id == 1;
}

function isManager(user) {
   return user && user.group_id == 2;
}

function isAdminOrManager(user) {
   return isAdmin(user) || isManager(user);
}

function photoByUserId(id, callback) {
   fs.readFile(__dirname+'/data/profile/'+id+'/photo', function (err, data) {
      if (err)
         callback(null);
      else
         callback(data);
   });
}

function editSelfPhoto(id, photo, callback) {
   fs.mkdir(__dirname+'/data/profile/'+id, function() {
      fs.writeFile(__dirname+'/data/profile/'+id+'/photo', photo, function(err, data) {
         console.log(err);
         callback(err == null);
      });
   });
}

var ready = function(application) {
   app = application;

   app.post('/web/reports/employees', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      db.query("select id, title from crm_task_category", null, function(err, data) {
         var taskCategoriesEntries = data.rows;
         db.query("select distinct position from crm_user order by position asc", null, function(err, data) {
            if(err) {
               console.log(err, data);
               return;
            }
            var positions = data.rows;
            var posCount = positions.length;
            var posQueried = 0;
            positions.forEach(function(entry, i) {
               db.query('select id, fname, name, oname, active from crm_user where position = $1 and active = true order by fname asc', [entry.position], function(err, data) {
                  entry.users = data.rows;
                  var usersCount = entry.users.length;

                  if(usersCount == 0) {
                     posQueried++;
                     if(posQueried == posCount) {
                        //console.log(JSON.stringify(positions, null, 3));
                        res.render('report_employees', { data: positions });
                     }
                     return;
                  }

                  var usersQueried = 0;
                  entry.users.forEach(function(userEntry, i) {
                     var catCount = taskCategoriesEntries.length;
                     var catQueried = 0;
                     userEntry.taskCategories = [];
                     userEntry.taskCount = 0;
                     taskCategoriesEntries.forEach(function(catEntry, i) {
                        //userEntry.taskCategories.push(catEntry);
                        db.query('select date_end, title from crm_task where employee_id = $1 and cat_id = $2 and complete = false order by title asc', [userEntry.id, catEntry.id],
                        function(err, data) {
                           if(!data) data = { rows: [] }
                           var taskEntries = data.rows;
                           var userCatEntry = { title: catEntry.title, tasks: [] };
                           userCatEntry.tasks = taskEntries;
                           userEntry.taskCount += taskEntries.length;
                           //console.log(catEntry.tasks);
                           userEntry.taskCategories.push(userCatEntry);
                           //if(userEntry.id == 1)
                           //   console.log('!!!!!',userEntry.taskCategories);
                           catQueried++;
                           //console.log('---catQueried ',catQueried,'/',catCount);
                           if(catQueried == catCount) {
                              usersQueried++;
                              //console.log('--usersQueried ',usersQueried,'/',usersCount);
                           }
                           if(usersQueried == usersCount) {
                              posQueried++;
                              //console.log('-posQueried ',posQueried,'/',posCount);
                           }
                           if(posQueried == posCount) {
                              //console.log(JSON.stringify(positions, null, 3));
                              res.render('report_employees', { data: positions });
                           }
                        });
                     });
                  });
               });
            });
         });
      });
   });

   app.post('/web/reports/tasks', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }

      /*Формат запроса: { userId, dateFrom, dateTo } */

      var userId = req.body.userId;
      var dateFrom = req.body.dateFrom;
      var dateTo = req.body.dateTo;
      //var dateFrom = new Date('2010-04-11');
      //var dateTo = new Date('2020-04-11')

      db.query("select id, title, date_start from crm_task where employee_id = $1 and date_start >= $2 and date_start <= $3",
      [userId, dateFrom, dateTo],
      function(err, data) {
         if(err) console.log(err);

         var starts = data.rows;

         db.query("select id, title, date_end from crm_task where complete = false and ((employee_id = $1) and ((date_end >= $2) and (date_end <= $3)))",
         [userId, dateFrom, dateTo],
         function(err, data) {
            if(err) console.log(err);

            var ends = data ? data.rows : { rows: [] };

            db.query("select id, title, date_complete, date_end from crm_task where complete = true and ((employee_id = $1) and ((date_complete >= $2) and (date_complete <= $3)))",
            [userId, dateFrom, dateTo],
            function(err, data) {

               var completes = data.rows;

               var history = [];
               starts.forEach(function(start, i) {
                  history.push({ id: start.id, title: start.title, date: start.date_start, type: 'start' });
               });
               completes.forEach(function(complete, i) {
                  history.push({ id: complete.id, title: complete.title, date: complete.date_complete, date_end: complete.date_end, type: 'complete' });
               });
               ends.forEach(function(end, i) {
                  history.push({ id: end.id, title: end.title, date: end.date_end, type: 'end' });
               });
               history.sort(function(a, b){
                  var keyA = new Date(a.date),
                  keyB = new Date(b.date);
                  if(keyA < keyB) return -1;
                  if(keyA > keyB) return 1;
                  return 0;
               });

               var datesHistory = [];
               history.forEach(function(entry,i) {
                  datesHistory.push({ date: entry.date, events: [
                     {
                        type: entry.type,
                        id: entry.id,
                        title: entry.title,
                        date_complete: entry.date,
                        date_end: entry.date_end ? entry.date_end : undefined
                     }
                  ] });
               });

               datesHistory.forEach(function(entry, i) {
                  if(!entry) return;
                  var date = entry.date;
                  for(var k = i+1; k < datesHistory.length; k++) {
                     var date2 = datesHistory[k].date;
                     if(date.getTime() == date2.getTime()) {
                        datesHistory[k].events.forEach(function(evt) {
                           entry.events.push(evt);
                        });
                        datesHistory[k] = null;
                     }
                  }
               });

               datesHistory.clean(undefined);

               var completeIds = [];

               datesHistory.forEach(function(entry, i) {
                  entry.events.forEach(function(evt) {
                     //if(evt.type == 'complete')
                     //   completeIds.push(evt.id);
                     if(evt.type == 'end') {
                        completes.forEach(function(completeEntry) {
                           if(completeEntry.id == evt.id)
                              evt.type = 'end (complete)';
                        });
                     }
                  });
               });
               //console.log(JSON.stringify(datesHistory, null, 3));
               db.query("select name, fname, oname, position from crm_user where id = $1", [userId],
               function(err, data) {
                  if(err) console.log(err);
                  var user = data.rows[0];
                  res.render('report_tasks', {
                     data: datesHistory,
                     user: user,
                     dateFrom: dateFrom,
                     dateTo: dateTo
                  });
               });
            });
         });
      });
   });

   app.post('/api/recoverPassword', function(req, res) {
      /*
         Формат запроса: { login }
         Формат ответа: { status: "ok" / "not exists" }
      */

      var login = req.body.login;
      if(!login) return;

      db.select().from('crm_user').where({ login: login }).rows(function(err, data) {
         var user = data[0];
         if(!user) {
            sendJson(res, { status: 'not exists' });
            return;
         }
         var code = randomString();
         db.insert('crm_pwrecovers', { user_id: user.id, code: code }).returning('*').row(function(err, data) {
            if(err) {
               console.log(err);
            }
            sendJson(res, { status: 'ok' });
            sendRecoverPasswordEmail(user.email, code);
         });
      });
   });

   app.post('/api/acceptRecoverPassword', function(req, res) {
      /*
         Формат запроса: { login, code, newPassword }
      */
      var login = req.body.login;
      var code = req.body.code;
      var newPassword = req.body.newPassword;

      db.select().from('crm_user').where({ login: login }).rows(function(err, data) {
         var user = data[0];
         if(!user) {
            res.send(403);
            return;
         }
         db.select().from('crm_pwrecovers').where({ user_id: user.id, code: code }).rows(function(err, data) {
            if(err) {
               console.log(err);
               res.send(500);
               return;
            }
            if(data[0]) {
               db.delete('crm_pwrecovers').where({ code: code }).run(function(err, data) { });
               db.update('crm_user', {
                  password_hash: md5(newPassword)
               }).where({ id: user.id }).run(function(err, data) {
                  if(err) console.log(err);
                  sendJson(res, { status: "ok" });
               });
            } else {
               sendJson(res, { status: "not exists" });
            }
         });
      });
   });

   app.post('/api/groups', function(req, res) {
      db.select().from('crm_user_group').rows(function(err, data) {
         if(err) return;
         sendJson(res, {groups: data});
      });
   });

   app.post('/api/taskCategories', function(req, res) {
      db.select().from('crm_task_category').rows(function(err, data) {
         if(err) return;
         sendJson(res, {categories: data});
      });
   });

   app.post('/api/myTaskCategories', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      userId = req.session.user.id;
      db.query("select crm_task_category.id, crm_task_category.title, count(crm_task) "
      +"from crm_task_category "
      +"left join crm_task on ((crm_task.cat_id = crm_task_category.id) and (crm_task.employee_id = $1) and (crm_task.complete = false)) "
      +"group by crm_task_category.id "
      +"order by count(crm_task) desc", [userId], function(err, data) {
         if(err) {
            console.log(err, data);
            sendJson(res, { categories: null });
            return;
         }
         sendJson(res, { categories: data.rows });
      });
   });

   app.post('/api/userTaskCategories', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      userId = req.body.userId;
      db.query("select crm_task_category.id, crm_task_category.title, count(crm_task) "
      +"from crm_task_category "
      +"left join crm_task on ((crm_task.cat_id = crm_task_category.id) and (crm_task.employee_id = $1) and (crm_task.complete = false)) "
      +"group by crm_task_category.id "
      +"order by count(crm_task) desc", [userId], function(err, data) {
         if(err) {
            console.log(err, data);
            sendJson(res, { categories: null });
            return;
         }
         sendJson(res, { categories: data.rows });
      });
   });

   app.post('/api/addTaskCategory', function(req, res) {
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      var cat = req.body;
      db.insert('crm_task_category', { title: cat.title }).returning('*').row(function(err, data) {
         sendJson(res, { status: 'ok' });
      });
   });

   app.post('/api/editTaskCategory', function(req, res) {
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      var cat = req.body;
      db.update('crm_task_category', {
         title: cat.title
      }).where({id: cat.id}).run(function(err, data) {
         if(err) console.log(err);
         sendJson(res, { status: 'ok' });
      });
   });

   app.post('/api/deleteTaskCategory', function(req, res) {
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      var cat = req.body;
      db.delete('crm_task_category').where({id: cat.id}).run(function(err, data) {
         if(err) console.log(err);
         sendJson(res, { status: 'ok' });
      });
   });

   app.post('/api/taskPriorities', function(req, res) {
      db.select().from('crm_task_priority').rows(function(err, data) {
         if(err) return;
         sendJson(res, {priorities: data});
      });
   });

   app.post('/api/emailExists', function(req, res) {
      var email = req.body.email;
      if(!email) {
         res.send(404);
         return;
      }
      findUserByEmail(email, function(user) {
         if(user) {
            sendJson(res, { status: 'ok' });
         } else {
            sendJson(res, { status: 'fail' });
         }
      })
   });

   app.post('/api/register', function(req, res) {
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      function verifyInput(user, groupId, callback) {
         var errors = [];
         if(!user)  {
            errors.push('user = null?');
            return error;
         }
         //login
         if(!user.login)
            errors.push('Укажите логин для пользователя');
         user.login = user.login ? user.login.trim() : '';
         if(user.login && user.login.length < 5) {
            errors.push('Слишком короткий логин. Должен быть >5 символов');
            callback(errors);
         }
         else {
            findUserByLogin(user.login, function(data) {
               if(data)
                  errors.push('Такой логин уже существует в системе');
               //name
               if(!user.name)
                  errors.push('Не указано имя');
               if(!user.fname)
                  errors.push('Не указанаа фамилия');
               if(!user.oname)

               //position
               if(!user.position)
                  errors.push('Не указана должность');

               //phone
               if(!user.phone)
                  errors.push('Не указан номер телефона');

               if(!user.email)
                  errors.push('Укажите email');

               //groupId
               if(!groupId)
                  errors.push('Укажите группу');
               callback(errors);
            });
         }
      };

      function addUser(user, groupId, callback) {
         var newPassword = randomString();
         var newUser = {};
         newUser.name = user.name;
         newUser.fname = user.fname;
         newUser.oname = user.oname;
         newUser.position = user.position;
         newUser.phone = user.phone;
         newUser.group_id = new Number(groupId);
         newUser.login = user.login;
         newUser.password_hash = md5(newPassword);
         newUser.email = user.email;
         if(user.other)
            newUser.other = user.other;
         db.insert('crm_user', newUser).returning('*').row(function(err, data) {
            callback({ login: newUser.login, password: newPassword });
            sendWelcomeEmail(newUser.email, newUser.login, newPassword );
         });
      };

      verifyInput(req.body, req.body.groupId, function(errors) {
         if(errors.length > 0) {
            sendJson(res, {status: 'fail', errors: errors});
         } else {
            addUser(req.body, req.body.groupId, function(newUser) {
               sendJson(res, {status: 'ok', errors: null});
            });
         }
      });
   });

   app.post('/api/login', function(req, res) {
      /*
         Формат запроса: { login, password }
         Формат ответа: { status: ok | fail }
      */
      if(!req.body.login || !req.body.password) {
         res.send(400);
         return;
      };
      db.select().from('crm_user').where({active: true, login: req.body.login, password_hash: md5(req.body.password) }).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         if(data.length > 0) {
            var user = data[0];
            req.session.user = user;
            sendJson(res, { status: 'ok', id: user.id, groupId: user.group_id});
         } else {
            req.session.user = null;
            sendJson(res, { status: 'fail'});
         }
      });
   });

   app.post('/api/self', function(req, res) {
      if(req.session.user) {
         var user = req.session.user;
         photoByUserId(user.id, function(photo) {
            sendJson(res, { loggedIn: true, user: {
                login: user.login
               ,name: user.name
               ,fname: user.fname
               ,oname: user.oname
               ,group_id: user.group_id
               ,photo: photo
            } });
         })
      } else {
         sendJson(res, { loggedIn: false });
      }
   });

   app.post('/api/user', function(req, res) {
      if(req.session.user) {
         var userId = req.body.userId;
         if(!userId) {
            res.send(400);
            return;
         }
         photoByUserId(userId, function(photo) {
            db.select('id,name,fname,oname,position,email,phone,group_id,login,active').from('crm_user').where({id: userId}).rows(function(err, data) {
               if(err) {
                  console.log(err);
                  return;
               }
               var user = data[0];
               sendJson(res, {
                   login: user.login
                  ,name: user.name
                  ,fname: user.fname
                  ,oname: user.oname
                  ,group_id: user.group_id
                  ,position: user.position
                  ,photo: photo
                  ,active: user.active
                  ,email: user.email
                  ,phone: user.phone
               });
            });
         })
      } else {
         res.send(403);
      }
   });

   app.post('/api/logout', function(req, res) {
      req.session.user = null;
      sendJson(res, { status: 'ok' });
   });

   app.post('/api/changePassword', function(req, res) {
      if(!req.session.user) return;
      var newPassword = req.body.password;
      var errors = [];
      if(!newPassword || newPassword.length < 6) {
         errors.push('Слишком короткий пароль. Попробуйте >6 символов');
         sendJson(res, { status: 'fail', errors: errors });
      } else {
         db.update('crm_user', { password_hash: md5(newPassword)}).where({ id: req.session.user.id }).run(function() {
            sendJson(res, { status: 'ok' });
         });
      }
   });

   app.post('/api/changeEmail', function(req, res) {
      if(!req.session.user) return;
      var newEmail = req.body.email;
      var errors = [];
      if(!newEmail || newEmail.length < 4) {
         errors.push('Неверный email');
         sendJson(res, { status: 'fail', errors: errors });
      } else {
         db.update('crm_user', { email: newEmail}).where({ id: req.session.user.id }).run(function() {
            sendJson(res, { status: 'ok' });
         });
      }
   });

   app.post('/api/changePhone', function(req, res) {
      if(!req.session.user) return;
      var newPhone = req.body.phone;
      var errors = [];
      if(!newPhone) {
         errors.push('Неверный номер телефона');
         sendJson(res, { status: 'fail', errors: errors });
      } else {
         db.update('crm_user', { phone: newPhone}).where({ id: req.session.user.id }).run(function() {
            sendJson(res, { status: 'ok' });
         });
      }
   });

   app.post('/api/changePhoto', function(req, res) {
      if(!req.session.user) return;
      var newPhoto = req.body.photo;
      var errors = [];
      editSelfPhoto(req.session.user.id, req.body.photo, function(success) {
         sendJson(res, { status: success ? 'ok' : 'fail', errors: errors });
      });
   });

   app.post('/api/users', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      var users = [];
      db.select('id,name,fname,oname,position,email,phone,group_id,login,active').from('crm_user').where({active:true}).rows(function(err, data) {
         if(err) return;
         sendJson(res, { users: data });
      });
   });

   app.post('/api/inactiveUsers', function(req, res) {
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      var users = [];
      db.select('id,name,fname,oname,position,email,group_id,login,active')
         .from('crm_user')
         .limit(50)
         .offset(req.body.offset)
         .where({active:false})
         .orderBy(['id desc'])
         .rows(function(err, data) {
         if(err) return;
         sendJson(res, { users: data });
      });
   });

   app.post('/api/editUser', function(req, res) {
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      var user = req.body;
      if(isAdmin(req.session.user)) {
         db.update('crm_user', {
            name: user.name
            ,fname: user.fname
            ,oname: user.oname
            ,position: user.position
            ,group_id: user.groupId
         }).where({id: user.id}).run(function(err, data) {
            if(err) console.log(err);
            sendJson(res, { status: 'ok' });
         });
      } else {
         db.update('crm_user', {
            name: user.name
            ,fname: user.fname
            ,oname: user.oname
            ,position: user.position
         }).where({id: user.id}).run(function(err, data) {
            if(err) console.log(err);
            sendJson(res, { status: 'ok' });
         });
      }
   });

   app.post('/api/activateUser', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var user = req.body;
      db.update('crm_user', {
         active: true
      }).where({id: user.id}).run(function(err, data) {
         if(err) console.log(err);
         sendJson(res, { status: 'ok' });
      });
   });

   app.post('/api/editTaskCategory', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var cat = req.body;
      db.update('crm_task_category', {
         title: cat.title
      }).where({id: cat.id}).run(function(err, data) {
         if(err) console.log(err);
         sendJson(res, { status: 'ok' });
      });
   });

   app.post('/api/deleteUser', function(req, res) {
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      var user = req.body;
      db.update('crm_user', {active: false}).where({id: user.id}).run(function(err, data) {
         if(err) console.log(err);
         sendJson(res, { status: 'ok' });
      });
   });

   app.post('/api/addTask', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var authorId = req.session.user.id;
      var employeeId = req.body.employeeId;
      var endDate = req.body.endDate || null;
      var title = req.body.title;
      var description = req.body.description;
      var priorityId = req.body.priorityId;
      var categoryId = req.body.categoryId;
      var attachedFiles = req.body.files;
      if(!employeeId || !description || !priorityId || !categoryId) {
         res.send(400);
         return;
      }
      db.insert('crm_task', {
         employee_id: employeeId
         , date_end: endDate
         , title: title
         , description: description
         , priority_id: priorityId
         , cat_id: categoryId
         , author_id: authorId
         , attached_files: '{'+attachedFiles+'}'
      }).returning('*').row(function(err, data) {
         if(err) {
            console.log(err);
            res.send(500);
            return;
         }
         sendJson(res, { status: 'ok'});
      });
   });

   app.post('/api/editTask', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var taskId = req.body.taskId;
      var text = req.body.text;
      if(!taskId || !text) {
         res.send(400);
         return;
      }

      db.update('crm_task', { description: text }).where({ id: taskId }).run(function(err, data) {
         if(err) {
            console.log(err);
         }
         sendJson(res, { status: 'ok' });
      });

   });

   app.post('/api/tasksByUserId', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      var employeeId = req.body.employeeId;
      var catId = req.body.categoryId;
      if(!employeeId || !catId) {
         res.send(400);
         return;
      };
      db.select().from('crm_task')
         .where({ employee_id: employeeId, cat_id: catId })
         .limit(50)
         .offset(req.body.offset)
         .orderBy(['complete', 'date_end asc']).rows(function(err, data) {
            if(err) return;
            sendJson(res, { tasks: data });
         });
   });

   app.post('/api/allTasksByUserId', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      var employeeId = req.body.employeeId;
      if(!employeeId) {
         res.send(400);
         return;
      };
      db.select().from('crm_task')
         .where({ employee_id: employeeId })
         .limit(50)
         .offset(req.body.offset)
         .orderBy(['complete', 'date_end asc']).rows(function(err, data) {
            if(err) return;
            sendJson(res, { tasks: data });
         });
   });

   app.post('/api/allTasksByAuthorId', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var authorId = req.body.authorId;
      if(!authorId) {
         res.send(400);
         return;
      };
      db.select().from('crm_task')
         .where({ author_id: authorId })
         .limit(50)
         .offset(req.body.offset)
         .orderBy(['complete', 'date_end asc']).rows(function(err, data) {
            if(err) return;
            sendJson(res, { tasks: data });
         });
   });

   app.post('/api/tasksByAuthorId', function(req, res) {
      if(!isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var authorId = req.body.authorId;
      var catId = req.body.categoryId;
      if(!authorId || !catId) {
         res.send(400);
         return;
      };
      db.select().from('crm_task')
         .where({ author_id: authorId, cat_id: catId })
         .limit(50)
         .offset(req.body.offset)
         .orderBy(['complete', 'date_end asc']).rows(function(err, data) {
            if(err) return;
            sendJson(res, { tasks: data });
         });
   });

   app.post('/api/liveTasksByAuthorId', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      var authorId = req.body.authorId;
      if(!authorId) {
         res.send(400);
         return;
      };
      db.select().from('crm_task')
         .where({ author_id: authorId, complete: false })
         .limit(50)
         .offset(req.body.offset)
         .orderBy(['complete', 'date_end asc']).rows(function(err, data) {
            if(err) return;
            sendJson(res, { tasks: data });
         });
   });

   app.post('/api/liveTasksByUserId', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      var employeeId = req.body.employeeId;
      if(!employeeId) {
         res.send(400);
         return;
      };
      db.select().from('crm_task')
         .where({ employee_id: employeeId, complete: false })
         .limit(50)
         .offset(req.body.offset)
         .orderBy(['date_end asc']).rows(function(err, data) {
            if(err) return;
            sendJson(res, { tasks: data });
         });
   });

   app.post('/api/completeTask', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      var employeeId = req.body.employeeId;
      var taskId = req.body.taskId;
      var complete = req.body.complete;
      function authTask(employeeId, taskId, callback) {
         db.select().from('crm_task').where({ id: taskId }).rows(function(err, data) {
            if(data) {
               var user = {id: employeeId}
               if(employeeId == req.session.user.id || isAdminOrManager(req.session.user)) {
                  callback(true, data[0]);
               } else {
                  callback(false);
               }
            } else {
               callback(false);
            }
         });
      };
      authTask(employeeId, taskId, function(auth, task) {
         if(auth) {
            db.update('crm_task', { complete: complete, date_complete: new Date() }).where({ id: taskId }).run(function(err, data) {
               if(err) {
                  console.log(err);
               }
               sendJson(res, { status: 'ok' });
               console.log(complete)
            });

            var requesterId = req.session.user.id;
            var employeeId = task.employee_id;
            if(requesterId != employeeId) {
               db.insert('crm_messages', {
                  id_empl_from: requesterId,
                  id_empl_to: employeeId,
                  text: 'Статус задачи '+task.title+' был изменен на: '+complete?'"Завершена"':'"В работе"',
                  title: 'Статус задачи изменен',
                  broadcast: true
               })
               .returning('*')
               .row(function(err, data) {});
            }
            var authorId = task.author_id;
            if(authorId != employeeId && complete) {
               db.insert('crm_messages', {
                  id_empl_from: requesterId,
                  id_empl_to: authorId,
                  text: 'Это автоматическое сообщение системы, уведомляющее вас о том, что сотрудник завершил поставленную задачу: '+task.title,
                  title: 'Уведомление. Завершена задача: '+task.title,
                  broadcast: true
               })
               .returning('*')
               .row(function(err, data) {});
            }
         } else {
            res.send(403);
         }
      });
   });

   app.post('/api/viewTask', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      var employeeId = req.body.employeeId;
      var taskId = req.body.taskId;
      if(!employeeId || !taskId) {
         res.send(400);
         return;
      };
      db.update('crm_task', { read: true }).where({ id: taskId }).run();
      db.select().from('crm_task').where({ id: taskId }).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         sendJson(res, data[0]);
      });
   });

   app.post('/api/postponeTask', function(req, res) {
      /*
         Формат запроса: { id, date }
         Формат ответа: 200
      */
      if(!req.session.user || !isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var taskId = req.body.id;
      var date = req.body.date;
      if(!taskId || !date) {
         res.send(400);
         return;
      };
      db.update('crm_task', { date_end: date }).where({ id: taskId }).run(function(err, data) {
         if(err) {
            console.log(err);
         }
      });
      db.select().from('crm_task').where({ id: taskId }).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         var task = data[0];
         var emplId = task.employee_id;

         db.insert('crm_messages', {
            id_empl_from: req.session.user.id,
            id_empl_to: task.employee_id,
            text: 'Задача '+task.title+' была отложена до '+formatDate(task.date_end),
            title: 'Уведомление: задача отложена',
            broadcast: true
         })
         .returning('*')
         .row(function(err, data) { });
      });

      res.send(200);
   });

   app.post('/api/priorityTask', function(req, res) {
      /*
         Формат запроса: { id, id_priority }
         Формат ответа: 200
      */
      if(!req.session.user || !isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      }
      var taskId = req.body.id;
      var priorityId = req.body.id_priority
      if(!taskId || !priorityId) {
         res.send(400);
         return;
      };
      db.update('crm_task', { priority_id: priorityId }).where({ id: taskId }).run(function(err, data) {
         if(err) {
            console.log(err);
         }
      });

      res.send(200);
   });

   app.post('/api/messages/send', function(req, res) {
      /*
         Формат:
         {
            employeeTo: ...,
            text: ...,
            title: ...,
            files: ...
         }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      var msg = req.body;
      if(!msg.employeeTo || !msg.text || !msg.title) {
         sendJson(res, { status: 'fail', errors: ['Неверный адресат или сообщение пусто'] });
         return;
      }
      db.insert('crm_messages', {
         id_empl_from: req.session.user.id,
         id_empl_to: msg.employeeTo,
         text: msg.text,
         title: msg.title,
         attached_files: '{'+msg.files+'}'
      })
      .returning('*')
      .row(function(err, data) {
         if(err) sendJson(res, { status: 'fail', errors: [err.message] });
         else sendJson(res, { status: 'ok' });
      });
   });

   app.post('/api/messages/broadcast', function(req, res) {
      /*
         Формат запроса:
         {
            title: ...
            text: ...
            files: ...
            users: ...
            addressAdmins: true || false
            addressManagers: true || false
            addressEmployees: true || false
         }
      */
      if(!req.session.user || !isAdminOrManager(req.session.user)) {
         res.send(403);
         return;
      };
      var msg = req.body;
      if(!msg.text || !msg.title) {
         sendJson(res, { status: 'fail', errors: ['Сообщение / заголовок пусто'] });
         return;
      }
      var addressAdmins = msg.addressAdmins == 'true';
      var addressManagers = msg.addressManagers == 'true';
      var addressEmployees = msg.addressEmployees == 'true';
      var receivers = [];

      db.select('id, group_id').from('crm_user').where({ active: true }).rows(function(err, data) {
         data.forEach(function(row) {
            var gId = row.group_id;
            if((gId == 1 && addressAdmins) || (gId == 2 && addressManagers) || (gId == 3 && addressEmployees))
               receivers.push(row.id);
         });
         var customUsers = req.body.users.split(',');
         receivers = uniqArray(customUsers[0] != '' ? receivers.concat(customUsers) : receivers);

         console.log('receivers = ', receivers);

         receivers.forEach(function(userId) {
            db.insert('crm_messages', {
               id_empl_from: req.session.user.id,
               id_empl_to: userId,
               text: msg.text,
               title: 'Публичная рассылка: '+msg.title,
               attached_files: '{'+msg.files+'}',
               broadcast: true
            })
            .returning('*')
            .row(function(err, data) {
               if(err) console.log(err);
            });
         });
      });

      sendJson(res, { status: 'ok' });

      /*db.insert('crm_messages', {
         id_empl_from: req.session.user.id,
         id_empl_to: msg.employeeTo,
         text: msg.text,
         title: msg.title,
         attached_files: '{'+msg.files+'}'
      })
      .returning('*')
      .row(function(err, data) {
         if(err) sendJson(res, { status: 'fail', errors: [err.message] });
         else sendJson(res, { status: 'ok' });
      });*/
   });

   app.post('/api/messages/read', function(req, res) {
      /*
         Формат запроса: { id }
         Формат ответа: { id, title, text, datetime }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      var id = req.body.id;
      var userId = req.session.user.id;
      if(!id) {
         res.send(400);
         return;
      }
      db.select().from('crm_messages').where({ id: id }).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         var msg = data[0];
         if(!msg) {
            res.send(404);
            return;
         }
         if(msg.id_empl_to == userId || msg.id_empl_from == userId) {
            if(msg.id_empl_to == userId)
               db.update('crm_messages', { read: true }).where({id: id}).run();
            sendJson(res, data[0]);
         } else {
            res.send(403);
         }
      });
   });

   app.post('/api/messages/archive', function(req, res) {
      /*
         Формат запроса: { id }
         Формат ответа: { id, title, text, datetime }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      var id = req.body.id;
      var userId = req.session.user.id;
      if(!id) {
         res.send(400);
         return;
      }
      db.select().from('crm_messages').where({ id: id }).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         var msg = data[0];
         if(!msg) {
            res.send(404);
            return;
         }
         if(msg.id_empl_to == userId) {
            db.update('crm_messages', { archive_to: true }).where({id: id}).run();
            sendJson(res, data[0]);
            return;
         } else {
            if(msg.id_empl_from == userId) {
               db.update('crm_messages', { archive_from: true }).where({id: id}).run();
               sendJson(res, data[0]);
               return;
            }
         }
      });
   });

   app.post('/api/messages/archiveAll', function(req, res) {
      /*
         Формат запроса: (none)
         Формат ответа: (none)
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      var userId = req.session.user.id;
      db.update('crm_messages', { archive_to: true }).where({ id_empl_to: userId }).run(function(err, data) {
         sendJson(res, {});
      });
   });

   app.post('/api/messages/archiveAllOutbox', function(req, res) {
      /*
      Формат запроса: (none)
      Формат ответа: (none)
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      var userId = req.session.user.id;
      db.update('crm_messages', { archive_from: true }).where({ id_empl_from: userId }).run(function(err, data) {
         sendJson(res, {});
      });
   });

   app.post('/api/messages/unarchive', function(req, res) {
      /*
         Формат запроса: { id }
         Формат ответа: { id, title, text, datetime }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      var id = req.body.id;
      var userId = req.session.user.id;
      if(!id) {
         res.send(400);
         return;
      }
      db.select().from('crm_messages').where({ id: id }).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         var msg = data[0];
         if(!msg) {
            res.send(404);
            return;
         }
         if(msg.id_empl_to == userId) {
            db.update('crm_messages', { archive_to: false }).where({id: id}).run();
            sendJson(res, data[0]);
            return;
         } else {
            if(msg.id_empl_from == userId) {
               db.update('crm_messages', { archive_from: false }).where({id: id}).run();
               sendJson(res, data[0]);
               return;
            }
         }
      });
   });

   app.post('/api/messages/viewArchive', function(req, res) {
      /*
         Формат запроса: { offset }
         Формат ответа: {
            messages: [
               { id_empl_from, id_empl_to, title, text, datetime }
            ]
         }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };

      db.query("select id, id_empl_to, id_empl_from, title, datetime, read, attached_files "
      +"from crm_messages "
      +"where ((id_empl_to = $1 AND archive_to = true) OR (id_empl_from = $1 AND archive_from = true)) "
      +"order by datetime desc "
      +"limit 50 offset $2", [req.session.user.id, req.body.offset], function(err, data) {
         if(err) {
            console.log(err, data);
            return;
         }
         sendJson(res, { messages: data.rows });
      });

      /*db.select('id, id_empl_to, id_empl_from, title, datetime, read, attached_files').from('crm_messages')
      .limit(50)
      .offset(req.body.offset)
      .where({ id_empl_to: req.session.user.id, archive_to: true })
      .orderBy(['read asc','datetime desc']).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         sendJson(res, {messages: data});
      });*/
   });

   app.post('/api/messages/inbox', function(req, res) {
      /*
         Формат запроса: { offset }
         Формат ответа: {
            messages: [
               { id_empl_from, id_empl_to, title, text, datetime }
            ]
         }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      db.select('id, id_empl_to, id_empl_from, title, datetime, read, attached_files').from('crm_messages')
      .limit(50)
      .offset(req.body.offset)
      .where({ id_empl_to: req.session.user.id, archive_to: false })
      .orderBy(['read asc','datetime desc']).rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         sendJson(res, {messages: data});
      });
   });

   app.post('/api/messages/outbox', function(req, res) {
      /*
         Формат запроса: { offset }
         Формат ответа: {
            messages: [
               { id_empl_from, id_empl_to, title, text, datetime }
            ]
         }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      db.select('id, id_empl_to, id_empl_from, title, datetime, read, attached_files').from('crm_messages')
      .limit(50)
      .offset(req.body.offset)
      .where({ id_empl_from: req.session.user.id, broadcast: false, archive_from: false })
      .orderBy(['read asc','datetime desc'])
      .rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         sendJson(res, {messages: data});
      });
   });

   app.post('/api/messages/dialog', function(req, res) {
      /*
         Формат запроса: { offset, employeeId }
         Формат ответа: {
            messages: [
               { id_empl_from, id_empl_to, title, text, datetime }
            ]
         }
      */
      if(!req.session.user) {
         res.send(403);
         return;
      };
      var employeeId = req.body.employeeId;
      var offset = req.body.offset;

      db.query("select id, id_empl_to, id_empl_from, title, datetime, read, attached_files, text "
      +"from crm_messages "
      +"where ((id_empl_from = $1 AND id_empl_to = $2) OR (id_empl_from = $2 AND id_empl_to = $1)) and broadcast = false "
      +"order by datetime desc "
      +"limit 50 offset $3", [req.session.user.id, employeeId, offset], function(err, data) {
         if(err) {
            console.log(err, data);
            return;
         }
         sendJson(res, { messages: data.rows });
      });
      db.query("update crm_messages set read = true "
      +"where ((id_empl_from = $1 AND id_empl_to = $2) OR (id_empl_from = $2 AND id_empl_to = $1)) and broadcast = false "
      +"order by datetime desc "
      +"limit 50 offset $3", [req.session.user.id, employeeId, offset], function(err, data) {});

      /*db.select('id, id_empl_to, id_empl_from, title, datetime, read, attached_files, text').from('crm_messages')
      .limit(50)
      .offset(offset)
      .where(db.sql.or({ id_empl_from: req.session.user.id, id_empl_to: employeeId, broadcast: false }, { id_empl_from: employeeId, id_empl_to: req.session.user.id, broadcast: false }))
      .orderBy(['read asc','datetime desc'])
      .rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         sendJson(res, {messages: data});
      });*/
   });

   app.post('/api/newEntries', function(req, res) {
      if(!req.session.user) {
         res.send(403);
         return;
      }
      userId = req.session.user.id;
      var news = {};
      function getUnreadTasks(userId, callback) {
         db.query("select count(id) from crm_task where employee_id = $1 and read = false", [userId], function(err, data) {
            if(err) {
               console.log(err);
               return;
            }
            callback(data.rows[0].count);
         });
      }
      function getUnreadMessages(userId, callback) {
         db.query("select count(id) from crm_messages where id_empl_to = $1 and read = false", [userId], function(err, data) {
            if(err) {
               console.log(err);
               return;
            }
            callback(data.rows[0].count);
         });
      }

      getUnreadTasks(userId, function(count) {
         news.newTasksCount = count;
         getUnreadMessages(userId, function(count) {
            news.newMessagesCount = count;
            sendJson(res, news);
         });
      });
   });

   app.post('/api/distinctPositions', function(req, res) {
      /*
         формат запроса:
         Формат ответа:
      */
      if(!req.session.user) {
         res.send(403);
         return;
      }
      db.query("select distinct position from crm_user where active = true", null, function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         sendJson(res, { positions: data.rows })
      });
   });

   app.use(function(req, res, next) {
      if(!req.session.user) {
         res.send(403);
      } else {
         next();
      }
   });

   app.use(function(req, res, next) {
      multer({
            dest: './filestorage/',
            onFileUploadComplete: function (file) {
               req.body.filepath = file.path;
               req.body.filesize = file.size;
            }
         })(req, res, next);
   });

   app.post('/file/upload', function(req, res) {
      db.insert('crm_files', {
         path: req.body.filepath,
         name: req.body.name,
         size: req.body.filesize,
         author_id: req.session.user.id
      }).returning('*').row(function(err, data) {
         //console.log(file.fieldname + ' uploaded to  ' + file.path)
         //sendJson(res, { status: 'ok' });
         res.send(200);
      });
   });

   app.post('/file/list', function(req, res) {
      /*
         Формат запроса: { offset }
         Формат ответа: {
            files: {
               [
                  id, name, uploadDate
               ]
            }
         }
      */
      db.select('id,name,uploadDate,size').from('crm_files')
         .orderBy('uploadDate desc')
         .limit(50)
         .offset(req.body.offset)
         .rows(function(err, data) {
            if(err) {
               console.log(err);
               return;
            }
            sendJson(res, {files: data});
         });
   });

   app.post('/file/byName', function(req, res) {
      /*
         Формат запроса: { name, offset }
         Формат ответа: {
            files: {
               [
                  id, name, uploadDate
               ]
            }
         }
      */
      db.select('id,name,uploadDate,size').from('crm_files')
         .where(db.sql.like('name', '%'+req.body.name+'%') )
         .orderBy('uploadDate desc')
         .limit(50)
         .offset(req.body.offset)
         .rows(function(err, data) {
            if(err) {
               console.log(err);
               return;
            }
            sendJson(res, {files: data});
         });
   });

   app.post('/file/my', function(req, res) {
      /*
         Формат запроса: { name, offset }
         Формат ответа: {
            files: {
               [
                  id, name, uploadDate
               ]
            }
         }
      */
      db.select('id,name,uploadDate,size').from('crm_files')
         .where({ author_id: req.session.user.id })
         .orderBy('uploadDate desc')
         .limit(50)
         .offset(req.body.offset)
         .rows(function(err, data) {
            if(err) {
               console.log(err);
               return;
            }
            sendJson(res, {files: data});
         });
   });

   app.post('/file/info', function(req, res) {
      /*
         Формат запроса: { id }
         Формат ответа: {
               id, name, uploadDate
            }
         }
      */
      db.select('id,name,uploadDate,size').from('crm_files')
         .where({ id: req.body.id })
         .rows(function(err, data) {
            if(err) {
               console.log(err);
               return;
            }
            sendJson(res, data[0]);
         });
   });

   app.post('/file/download', function(req, res) {
      /*
         Формат запроса: { id }
      */
      var id = req.body.id;
      if(!id) {
         res.send(400);
         return;
      }
      db.select('name, path').from('crm_files')
      .where({ id: id })
      .rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         var file = data[0];
         if(!file) {
            res.send(404);
            return;
         } else {
            var filename = './'+file.path;
            res.sendfile(filename, null);
         }
      });
   });

   app.post('/file/delete', function(req, res) {
      /*
         Формат запроса: { id }
         Формат ответа: { status }
      */
      if(!isAdmin(req.session.user)) {
         res.send(403);
         return;
      }
      var id = req.body.id;
      db.select('name, path').from('crm_files')
      .where({ id: id })
      .rows(function(err, data) {
         if(err) {
            console.log(err);
            return;
         }
         var file = data[0];
         if(!file) {
            res.send(404);
            return;
         } else {
            var filename = './'+file.path;
            fs.unlink(filename);
            db.delete('crm_files').where({id: id}).run(function(err, data) {
               if(err) console.log(err);
               sendJson(res, { status: 'ok' });
            });
         }
      });
   });
}

module.exports = { callback: ready, db: db };

function randomString() {
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

   for( var i=0; i < 25; i++ )
     text += possible.charAt(Math.floor(Math.random() * possible.length));
   return text;
}

function formatDate(date) {
   return date;
}

function uniqArray(a) {
    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}

Array.prototype.clean = function(deleteValue) {
   for (var i = 0; i < this.length; i++) {
      if (this[i] == deleteValue) {
         this.splice(i, 1);
         i--;
      }
   }
   return this;
};
